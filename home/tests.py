from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
import os
import time
from . import views
from django.contrib.auth.models import User

class UnitTest(TestCase):
    def test_status_url(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_landing(self):
        response = resolve('/')
        self.assertEqual(response.func, views.landing)

    def test_function_login(self):
        response = resolve('/login/')
        self.assertEqual(response.func, views.logins)
    
    def test_function_logout(self):
        response = resolve('/logout/')
        self.assertEqual(response.func, views.logouts)

    def test_function_register(self):
        response = resolve('/register/')
        self.assertEqual(response.func, views.registers)
    
    def test_template(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'landing.html')
    


