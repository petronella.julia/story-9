from django.urls import path
from . import views

app_name = 'home'
urlpatterns = [
    path('', views.landing, name='landing'),
    path('login/', views.logins, name='login'),
    path('logout/', views.logouts, name='logout'),
    path('register/', views.registers, name='register')
]
