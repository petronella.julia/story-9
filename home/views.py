from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout, authenticate

# Create your views here.
def landing(req):
    error = "Username or Password is wrong"
    user = req.user
    if user.is_authenticated:
        return redirect('home:login')
    if req.method == 'POST':
        form = AuthenticationForm(data=req.POST)
        if form.is_valid():
            user = form.get_user()
            login(req, user)
            return redirect('home:login')
    else:
        form = AuthenticationForm()
    return render(req, 'landing.html', {'form':form, 'error':error})

def logins(req):
    return render(req, 'register.html')

def logouts(req):
    logout(req)
    return redirect('home:landing')

def registers(req):
    if req.method == 'POST':
        form = UserCreationForm(req.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(req, user)
            return redirect('home:landing')
    else:
        form = UserCreationForm()
    return render(req, 'request.html', {'form': form})


